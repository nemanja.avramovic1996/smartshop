package hr.ferit.nemanjaavramovic.smartshop.AdminActivities

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import hr.ferit.nemanjaavramovic.smartshop.Kupac.KupacAdapter
import hr.ferit.nemanjaavramovic.smartshop.Kupac.KupacDao
import hr.ferit.nemanjaavramovic.smartshop.Kupac.KupacDatabase
import hr.ferit.nemanjaavramovic.smartshop.Kupac.KupacInteractionListener
import hr.ferit.nemanjaavramovic.smartshop.R
import kotlinx.android.synthetic.main.activity_trenutno_u_trgovini.*

class TrenutnoUTrgoviniActivity : AppCompatActivity() {

    companion object {
        lateinit var kupacDatabase: KupacDao
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        setContentView(R.layout.activity_trenutno_u_trgovini)
        inicijalizacija()
        prikaziKupce()

    }

    @SuppressLint("WrongConstant")
    private fun inicijalizacija() {

        recycler_view_kupci.layoutManager = LinearLayoutManager(this,LinearLayout.VERTICAL,false)


    }

    private fun prikaziKupce() {
        val kupciListener = object:
            KupacInteractionListener {
        }

        recycler_view_kupci.adapter=
            KupacAdapter(
                kupacDatabase.getAll(),
                kupciListener
            )
    }

    override fun onBackPressed() {
        var sviKupci = kupacDatabase.getAll()
        for (k in sviKupci){
            kupacDatabase.delete(k)
        }
        startActivity(
            Intent(this@TrenutnoUTrgoviniActivity,
            AdminActivity::class.java)
        )
    }


}
