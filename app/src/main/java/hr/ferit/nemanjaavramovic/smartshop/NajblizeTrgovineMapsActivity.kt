package hr.ferit.nemanjaavramovic.smartshop

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class NajblizeTrgovineMapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_najblize_trgovine_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
        val FERITShop = LatLng(45.54, 18.69)
        val FERShop = LatLng(45.8, 15.97)
        val FESBShop = LatLng(43.5, 16.46)
        val FOIShop = LatLng(46.3, 16.33)
        val RiTehShop = LatLng(45.32, 14.46)
        val Croatia = LatLng(45.33, 16.37)


        mMap.addMarker(MarkerOptions().position(FERITShop).title("FERIT SmartShop"))
        mMap.addMarker(MarkerOptions().position(FERShop).title("FER SmartShop"))
        mMap.addMarker(MarkerOptions().position(FESBShop).title("FESB SmartShop"))
        mMap.addMarker(MarkerOptions().position(FOIShop).title("FOI SmartShop"))
        mMap.addMarker(MarkerOptions().position(RiTehShop).title("RiTeh SmartShop"))
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(FERITShop))
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Croatia, 6F),3000,null)
    }
}
