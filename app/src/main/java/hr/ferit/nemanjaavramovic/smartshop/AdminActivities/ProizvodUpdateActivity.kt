package hr.ferit.nemanjaavramovic.smartshop.AdminActivities

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import hr.ferit.nemanjaavramovic.smartshop.Proizvod.Proizvod
import hr.ferit.nemanjaavramovic.smartshop.R
import kotlinx.android.synthetic.main.activity_proizvod_update.*
import java.math.RoundingMode

class ProizvodUpdateActivity : AppCompatActivity() {

    companion object {
        const val PROIZVOD_ID: String = "id"
        const val PROIZVOD_IME: String = "ime"
        const val PROIZVOD_SLIKA: String = "slika"
        const val PROIZVOD_CIJENA: String = "cijena"
    }

    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        setContentView(R.layout.activity_proizvod_update)
        inicijalizacija()

    }

    private fun inicijalizacija() {
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference.child("Proizvodi")
        val idProizvoda = intent?.getStringExtra(PROIZVOD_ID)
        val imeProizvoda = intent?.getStringExtra(PROIZVOD_IME)
        val slikaProizvoda = intent?.getStringExtra(PROIZVOD_SLIKA)
        val cijenaProizvoda = intent?.getDoubleExtra(PROIZVOD_CIJENA,0.0)
        Picasso.get().load(slikaProizvoda).into(slikaProizvodaImageView)
        imeProizvodaUpdate.setText(imeProizvoda)
        slikaProizvodaUpdate.setText(slikaProizvoda)
        cijenaProizvodaUpdate.setText(cijenaProizvoda.toString())
        updateBtn.setOnClickListener{updateProizvod(idProizvoda)}
        obrisiBtn.setOnClickListener{obrisiProizvod(idProizvoda)}


    }

    private fun obrisiProizvod(idProizvoda: String?) {
        mDatabaseReference?.addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {
                for (k in snapshot.children) {
                    if (k.key == idProizvoda.toString()){
                        mDatabaseReference = mDatabase!!.reference.child("Proizvodi")
                        k.key?.let { mDatabaseReference!!.child(it).removeValue() }
                        break
                    }

                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
        otvoriAdminActivity("o")
    }

    private fun otvoriAdminActivity(s: String) {
        if (s == "o"){
            Toast.makeText(this@ProizvodUpdateActivity, "Proizvod je uspješno obrisan iz baze svih proizvoda",
                Toast.LENGTH_LONG).show()
        }else{
            Toast.makeText(this@ProizvodUpdateActivity, "Proizvod je uspješno ažuriran",
                Toast.LENGTH_LONG).show()
        }

        startActivity(
            Intent(this@ProizvodUpdateActivity,
            AdminActivity::class.java)
        )
    }

    private fun updateProizvod(idProizvoda: String?) {
        mDatabaseReference?.addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {
                for (k in snapshot.children) {
                    if (k.key == idProizvoda.toString()){
                        val proizvod =
                            Proizvod(
                                imeProizvodaUpdate.text.toString(),
                                slikaProizvodaUpdate.text.toString(),
                                cijenaProizvodaUpdate.text.toString().toBigDecimal().setScale(2, RoundingMode.UP)
                                    .toDouble()
                            )
                        val map_proizvod = proizvod.toMap()
                        mDatabaseReference = mDatabase!!.reference.child("Proizvodi")
                        k.key?.let { mDatabaseReference!!.child(it).setValue(map_proizvod) }
                        break
                    }

                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
        otvoriAdminActivity("u")
    }
}
