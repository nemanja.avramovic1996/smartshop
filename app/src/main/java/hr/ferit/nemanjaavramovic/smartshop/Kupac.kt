package hr.ferit.nemanjaavramovic.smartshop

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "kupci")
data class Kupac(
    @PrimaryKey(autoGenerate = true) var id: Int,
    @ColumnInfo(name = "kupacIme") var kupacIme: String,
    @ColumnInfo(name = "kupacPrezime") var kupacPrezime: String
)