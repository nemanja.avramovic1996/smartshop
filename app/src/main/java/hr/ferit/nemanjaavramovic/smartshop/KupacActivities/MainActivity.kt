package hr.ferit.nemanjaavramovic.smartshop.KupacActivities

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.zxing.integration.android.IntentIntegrator
import hr.ferit.nemanjaavramovic.smartshop.LoginActivity
import hr.ferit.nemanjaavramovic.smartshop.Proizvod.ProizvodKosaricaDatabase
import hr.ferit.nemanjaavramovic.smartshop.R
import hr.ferit.nemanjaavramovic.smartshop.CallbackFunction
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    //Firebase reference
    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabaseReference2: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null
    private var mAuth: FirebaseAuth? = null

    val proizvodKosaricaDatabase = ProizvodKosaricaDatabase.getInstance().proizvodKosaricaDao()

    var resultQR: String =""

    //varijable elementata
    private var tvIme: TextView? = null
    private var tvStanje: TextView? = null
    //private var tvEmail: TextView? = null
    private var buttonOdjava: ImageButton? = null
    private var buttonKosarica: ImageButton? = null
    private var buttonScan: ImageButton? = null
    private var buttonMaps: ImageButton? = null

    companion object{
        var imeProizvoda = ""
        var slikaProizvoda = ""
        var cijenaProizvoda = ""
        var flag : Int = 0
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        setContentView(R.layout.activity_main)

        inicijalizacija()

    }

    private fun inicijalizacija() {
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("Korisnici")
        mDatabaseReference2 = mDatabase!!.reference!!.child("Proizvodi")
        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance()
        tvIme = findViewById<View>(R.id.tv_ime) as TextView
        tvStanje = findViewById<View>(R.id.tv_stanje) as TextView
        buttonOdjava = findViewById<View>(R.id.odjavaBtn) as ImageButton
        buttonKosarica = findViewById<View>(R.id.kosaricaBtn) as ImageButton
        buttonScan = findViewById<View>(R.id.qrScanBtn) as ImageButton
        buttonMaps = findViewById<View>(R.id.najblizeTrgovineMapsBtn) as ImageButton
        buttonOdjava?.setOnClickListener { odjaviteSe() }
        buttonScan?.setOnClickListener {
            val scanner = IntentIntegrator(this)
            scanner.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE)
            scanner.setBeepEnabled(true)
            scanner.initiateScan()
        }
        buttonKosarica?.setOnClickListener{otvoriKosaricu()}

        buttonMaps?.setOnClickListener{pogledajNajblizeTrgovine()}


        val korisnikID = mAuth!!.currentUser!!.uid
        val trenutniKorisnikDb = mDatabaseReference!!.child(korisnikID)

        trenutniKorisnikDb.child("trenutno_u_trgovini").setValue("+")


    }

    private fun pogledajNajblizeTrgovine() {
        startActivity(Intent(this@MainActivity,
            NajblizeTrgovineActivity::class.java))
    }

    private fun otvoriKosaricu() {
        startActivity(Intent(this@MainActivity,
            KosaricaActivity::class.java))
    }

    private fun odjaviteSe(){
        val num = proizvodKosaricaDatabase.getAll().size
        if(num > 0){
            Toast.makeText(this@MainActivity, "Ne možete se odjaviti ni izaći iz trgovine dok vam je košarica puna. Ili vratite proizvode iz košarice ili platite odabrane proizvode!",
                Toast.LENGTH_LONG).show()
        }
        else{
            val korisnikID = mAuth!!.currentUser!!.uid
            val trenutniKorisnikDb = mDatabaseReference!!.child(korisnikID)

            trenutniKorisnikDb.child("trenutno_u_trgovini").setValue("-")
            resultQR  = ""
            mAuth?.signOut()

            startActivity(
                Intent(this@MainActivity,
                    LoginActivity::class.java)
            )
        }


    }

    override fun onStart() {
        super.onStart()
        val mUser = mAuth!!.currentUser
        val mUserReference = mDatabaseReference!!.child(mUser!!.uid)
        mUserReference.addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {
                tvIme?.text  = snapshot.child("ime").value as String
                tvStanje?.text = snapshot.child("racun").value as String + " kn"
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    override fun onBackPressed() {
        Toast.makeText(this@MainActivity, "Ne mozete ici unazad, prvo se morate odjaviti",
            Toast.LENGTH_LONG).show()
    }

    fun procitajVrijednostCallbacka(callbackFunction: CallbackFunction) {
        mDatabaseReference2?.addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {
                flag = 2
                for (k in snapshot.children) {
                    flag = 0
                    if (k.key == resultQR){
                        imeProizvoda = k.child("imeProizvoda").value.toString()
                        slikaProizvoda = k.child("slikaProizvoda").value.toString()
                        cijenaProizvoda = k.child("cijenaProizvoda").value.toString()
                        flag = 1;
                        break
                    }
                }
                callbackFunction.onCallback(flag)
            }
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })

    }
    private fun toastMessage(flag: Int) {
        if (flag == 0){
            Toast.makeText(this, "Ne postoji proizvod pod tim ID-em", Toast.LENGTH_SHORT).show()
        }
        else if (flag == 1){
            Toast.makeText(this, "Proizvod u bazi", Toast.LENGTH_SHORT).show()
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        resultQR = ""
        flag = 2
        if (resultCode == Activity.RESULT_OK) {
            var result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
            if (result != null) {
                if (result.contents == null) {
                    Toast.makeText(this, "Odbijeno", Toast.LENGTH_LONG).show()
                } else {
                    resultQR = result.contents.toString()

                    procitajVrijednostCallbacka(object : CallbackFunction {
                        override fun onCallback(flag: Int) {
                            toastMessage(flag)
                        }
                    })

                    Handler().postDelayed(Runnable {
                        if(flag == 1){
                            otvoriProizvod(imeProizvoda, slikaProizvoda, cijenaProizvoda)
                        }
                    }, 2000)


    

                }
            } else {
                super.onActivityResult(requestCode, resultCode, data)
                Toast.makeText(this, "QR kod nije valjan", Toast.LENGTH_LONG).show()
            }
        }
        else{
            Toast.makeText(this, "QR kod nije valjan", Toast.LENGTH_LONG).show()
        }


    }



    private fun otvoriProizvod(ime: String, slika: String, cijena: String) {
        val displayIntent = Intent(this, ItemActivity::class.java)
        displayIntent.putExtra(ItemActivity.PROIZVOD_IME, ime)
        displayIntent.putExtra(ItemActivity.PROIZVOD_SLIKA, slika)
        displayIntent.putExtra(ItemActivity.PROIZVOD_CIJENA, cijena)
        startActivity(displayIntent)
    }

}
