package hr.ferit.nemanjaavramovic.smartshop

import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ForgotPasswordActivity : AppCompatActivity() {

    private val TAG = "ForgotPasswordActivity"
    private var elementEmail: EditText? = null
    private var buttonResetLozinke: Button? = null
    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        setContentView(R.layout.activity_forgot_password)

        inicijalizacija()
    }

    private fun inicijalizacija() {
        elementEmail = findViewById<View>(R.id.emailR) as EditText
        buttonResetLozinke = findViewById<View>(R.id.btn_resetLozinke) as Button
        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance()
        buttonResetLozinke!!.setOnClickListener { posaljiEmailZaResetLozinke() }
    }

    private fun posaljiEmailZaResetLozinke() {
        val email = elementEmail?.text.toString()
        if (!TextUtils.isEmpty(email)) {
            mAuth!!
                .sendPasswordResetEmail(email)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val poruka = "Email poslan."
                        Log.d(TAG, poruka)
                        Toast.makeText(this, poruka, Toast.LENGTH_SHORT).show()
                        updateUI()
                    } else {
                        Log.w(TAG, task.exception!!.message)
                        Toast.makeText(this, "Nema korisnika s tim emailom.", Toast.LENGTH_SHORT).show()
                    }
                }
        } else {
            Toast.makeText(this, "Unesi email", Toast.LENGTH_SHORT).show()
        }
    }
    private fun updateUI() {
        val intent = Intent(this@ForgotPasswordActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }
}
