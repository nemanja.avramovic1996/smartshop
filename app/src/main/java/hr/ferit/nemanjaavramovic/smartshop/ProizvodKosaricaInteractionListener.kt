package hr.ferit.nemanjaavramovic.smartshop


interface ProizvodKosaricaInteractionListener {
    fun onDelete(proizvodIme: String)
    fun onUpdate(proizvodIme: String, number: String)
}