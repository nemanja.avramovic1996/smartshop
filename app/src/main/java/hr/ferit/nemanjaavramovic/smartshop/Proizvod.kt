package hr.ferit.nemanjaavramovic.smartshop

import com.google.firebase.database.Exclude

data class Proizvod(
    val id: Int,
    val imeProizvoda: String,
    val slikaProizvoda: String,
    val cijenaProizvoda: Double
    //val buttonDesign: Int,
    //val theme: Int
    ){

    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "id" to id,
            "imeProizvoda" to imeProizvoda,
            "slikaProizvoda" to slikaProizvoda,
            "cijenaProizvoda" to cijenaProizvoda
        )
    }
}