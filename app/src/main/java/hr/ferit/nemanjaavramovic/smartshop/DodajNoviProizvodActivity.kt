package hr.ferit.nemanjaavramovic.smartshop

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import hr.ferit.nemanjaavramovic.smartshop.TrenutnoUTrgoviniActivity.Companion.kupacDatabase
import kotlinx.android.synthetic.main.activity_dodaj_novi_proizvod.*
import java.math.RoundingMode

class DodajNoviProizvodActivity : AppCompatActivity() {


    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null
    private var mAuth: FirebaseAuth? = null


    private lateinit var IDProizvoda: EditText
    private lateinit var imeProizvoda: EditText
    private lateinit var slikaProizvoda: EditText
    private lateinit var cijenaProizvoda: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dodaj_novi_proizvod)

        inicijalizacija()

    }

    private fun inicijalizacija() {
        IDProizvoda = findViewById<View>(R.id.idProizvoda) as EditText
        imeProizvoda = findViewById<View>(R.id.imeProizvoda) as EditText
        slikaProizvoda = findViewById<View>(R.id.slikaProizvoda) as EditText
        cijenaProizvoda = findViewById<View>(R.id.cijenaProizvoda) as EditText

        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!
        FirebaseApp.initializeApp(this);
        var mDatabaseReference2 = mDatabaseReference!!.child("Proizvodi")
        mDatabaseReference2?.addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {
                var u = 0
                for (k in snapshot.children) {
                    if (k.child("id").value.toString() == IDProizvoda.text.toString()){
                        u++
                    }
                }
                flag.text = "1"
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })
        dodajBtn.setOnClickListener{dodajProizvod()}
    }


    private fun dodajProizvod() {
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!
        FirebaseApp.initializeApp(this);





        if (!TextUtils.isEmpty(IDProizvoda.text.toString()) && !TextUtils.isEmpty(imeProizvoda.text.toString())
            && !TextUtils.isEmpty(slikaProizvoda.text.toString()) && !TextUtils.isEmpty(cijenaProizvoda.text.toString())) {


            if (flag.text.toString() == "0"){
                val data_proizvod = mDatabaseReference!!.child("Proizvodi").push()
                val proizvod = Proizvod(IDProizvoda.text.toString().toInt(), imeProizvoda.text.toString(), slikaProizvoda.text.toString(), cijenaProizvoda.text.toString().toBigDecimal().setScale(2, RoundingMode.UP).toDouble())
                val map_proizvod = proizvod.toMap()
                data_proizvod.setValue(map_proizvod)
                Toast.makeText(this, "Dodan je novi proizvod", Toast.LENGTH_SHORT).show()
                startActivity(
                    Intent(this@DodajNoviProizvodActivity,
                        AdminActivity::class.java)
                )
            }else{
                Toast.makeText(this, "Proizvod s tim ID-em postoji, promijenite ID", Toast.LENGTH_SHORT).show()
                flag.text = "0"
            }
        }else {
            Toast.makeText(this, "Popunite sva tražena polja", Toast.LENGTH_SHORT).show()
        }

    }


}



