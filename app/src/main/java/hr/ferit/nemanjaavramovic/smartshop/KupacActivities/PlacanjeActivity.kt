package hr.ferit.nemanjaavramovic.smartshop.KupacActivities

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import hr.ferit.nemanjaavramovic.smartshop.Proizvod.ProizvodKosaricaDatabase
import hr.ferit.nemanjaavramovic.smartshop.R
import hr.ferit.nemanjaavramovic.smartshop.TransakcijaPopis.Transakcija
import kotlinx.android.synthetic.main.activity_placanje.*
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.util.*

class PlacanjeActivity : AppCompatActivity() {

    companion object {
        const val UKUPNA_CIJENA: String = "ukupna_cijena"
    }
    val proizvodKosaricaDatabase = ProizvodKosaricaDatabase.getInstance().proizvodKosaricaDao()

    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null
    private var mAuth: FirebaseAuth? = null

    private  val TAG = MainActivity::getLocalClassName.toString()

    private  lateinit var biometricPromt: BiometricPrompt
    private lateinit var promtInfo: BiometricPrompt.PromptInfo

    private  lateinit var biometricManager: BiometricManager



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        setContentView(R.layout.activity_placanje)
        inicijalizacija()
    }

    private fun inicijalizacija() {
        val ukupnaCijena = intent?.getDoubleExtra(UKUPNA_CIJENA.toString(), 0.0)
        val upit = findViewById<TextView>(R.id.upitTxt)
        upit.text = "Da li ste sigurni da želite završiti s kupovinom i platiti " + ukupnaCijena.toString() + "kn?"

        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("Korisnici")
        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance()

        var mUser = mAuth!!.currentUser
        var mUserReference = mDatabaseReference!!.child(mUser!!.uid)
        mUserReference.addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {
                trenutnoStanje.text = snapshot.child("racun").value as String
                stanjeTxt.text = "Trenutno stanje na vašem računu je " + trenutnoStanje.text + "kn"
                imeCurrent.text = snapshot.child("ime").value as String
                prezimeCurrent.text = snapshot.child("prezime").value as String

            }
            override fun onCancelled(databaseError: DatabaseError) {}

        })

        var mAdminReference = mDatabaseReference!!.child("1eqTJGacDdQMXV0Eo590xa2t6cz2")
        mAdminReference.addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {
                trenutnoStanjeadmin.text = snapshot.child("racun").value as String

            }
            override fun onCancelled(databaseError: DatabaseError) {}

        })

        val plati = findViewById<Button>(R.id.zavrsi_placanjeBtn)

        biometricManager = BiometricManager.from(this)
        val executor = ContextCompat.getMainExecutor(this)

        checkBiometricStatus(biometricManager)

        biometricPromt = BiometricPrompt(this,executor,
            object: BiometricPrompt.AuthenticationCallback(){
                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)

                    if (errorCode == 12){
                        zavrsiPlacanje(ukupnaCijena)
                    }
                    else{
                        Toast.makeText(this@PlacanjeActivity, "$errString",
                        Toast.LENGTH_LONG).show()
                    }

                }

                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    zavrsiPlacanje(ukupnaCijena)
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    Toast.makeText(this@PlacanjeActivity, "Autentifikacija neuspješna",
                        Toast.LENGTH_LONG).show()
                }
            })

        promtInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("Biometrijska sigurnost")
            .setDescription("Prislonite prst na otisak prsta ili skenirajte lice za uspješnu autentifikaciju")
            .setNegativeButtonText("Odustanite")
            .build()

        plati.setOnClickListener{
            biometricPromt.authenticate(promtInfo)
        }
    }



    private fun checkBiometricStatus(biometricManager: BiometricManager){
        when(biometricManager.canAuthenticate()){
            BiometricManager.BIOMETRIC_SUCCESS->
                Log.d(TAG, "checkBiometricStatus: App can use biometric authenticate")
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE->
                Log.d(TAG, "checkBiometricStatus: No biometric feautures available in the device")
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE->
                Log.d(TAG, "checkBiometricStatus: Biometric feautures currently unvailable")
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED->
                Log.d(TAG, "checkBiometricStatus: The user hasn't assoicate with any biometric")
        }
    }



    private fun zavrsiPlacanje(ukupnaCijena: Double?) {

        if (trenutnoStanje.text.toString().toDouble() < ukupnaCijena!!){
            Toast.makeText(this@PlacanjeActivity, "Nemate dovoljno novca na računu za kupovinu",
                Toast.LENGTH_LONG).show()
        }
        else{
            var novoStanje : Double = 0.0
            var novoStanjeAdmin : Double = 0.0

            upisUTransakcije(ukupnaCijena)

            novoStanje = trenutnoStanje.text.toString().toDouble() - ukupnaCijena!!
            novoStanje = novoStanje.toBigDecimal().setScale(2, RoundingMode.UP).toDouble()

            novoStanjeAdmin = trenutnoStanjeadmin.text.toString().toDouble() + ukupnaCijena!!
            novoStanjeAdmin = novoStanjeAdmin.toBigDecimal().setScale(2, RoundingMode.UP).toDouble()

            val korisnikID = mAuth!!.currentUser!!.uid
            val trenutniKorisnikDb = mDatabaseReference!!.child(korisnikID)
            val adminKorisnikDb = mDatabaseReference!!.child("1eqTJGacDdQMXV0Eo590xa2t6cz2")

            trenutniKorisnikDb.child("racun").setValue(novoStanje.toString())
            adminKorisnikDb.child("racun").setValue(novoStanjeAdmin.toString())

            val num = proizvodKosaricaDatabase.getAll()
            var i = 0
            while(i < num.size){
                proizvodKosaricaDatabase.delete(num[i])
                i++
            }

            Toast.makeText(this@PlacanjeActivity, "Plaćanje uspješno, možete izaći iz trgovine",
                Toast.LENGTH_LONG).show()

            val displayIntent = Intent(this@PlacanjeActivity, MainActivity::class.java)
            startActivity(displayIntent)
        }



    }

    private fun upisUTransakcije(ukupnaCijena: Double) {

        val datumIVrijeme = Calendar.getInstance()
        val datum = SimpleDateFormat("MMMM d, Y").format(datumIVrijeme.time)
        val vrijeme = SimpleDateFormat("H:m:s").format(datumIVrijeme.time)
        var mDatabase2 = FirebaseDatabase.getInstance()
        var mDatabaseReference2 = mDatabase!!.reference!!.child("Transakcije")

        val data_transakcija = mDatabaseReference2.push()
        val transakcija =
            Transakcija(
                imeCurrent.text.toString(),
                prezimeCurrent.text.toString(),
                ukupnaCijena,
                datum,
                vrijeme
            )
        val map_transakcija = transakcija.toMap()
        data_transakcija.setValue(map_transakcija)
    }
}
