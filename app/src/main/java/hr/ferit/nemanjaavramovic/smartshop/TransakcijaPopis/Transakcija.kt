package hr.ferit.nemanjaavramovic.smartshop.TransakcijaPopis

import com.google.firebase.database.Exclude



data class Transakcija(
    val imeKupca: String,
    val prezimeKupca: String,
    val ukupnaCijena: Double,
    val datum: String,
    val vrijeme: String
){

    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "imeKupca" to imeKupca,
            "prezimeKupca" to prezimeKupca,
            "ukupnaCijena" to ukupnaCijena,
            "datum" to datum,
            "vrijeme" to vrijeme
        )
    }
}