package hr.ferit.nemanjaavramovic.smartshop

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_kosarica.*
import kotlinx.android.synthetic.main.activity_trenutno_u_trgovini.*

class TrenutnoUTrgoviniActivity : AppCompatActivity() {

    companion object {
        lateinit var kupacDatabase: KupacDao
    }




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trenutno_u_trgovini)
        inicijalizacija()
        prikaziKupce()

    }

    @SuppressLint("WrongConstant")
    private fun inicijalizacija() {
        kupacDatabase = KupacDatabase.getInstance().kupacDao()
        recycler_view_kupci.layoutManager = LinearLayoutManager(this,LinearLayout.VERTICAL,false)


    }

    private fun prikaziKupce() {
        val kupciListener = object: KupacInteractionListener{
        }

        recycler_view_kupci.adapter= KupacAdapter(kupacDatabase.getAll(),kupciListener)
    }

    override fun onBackPressed() {
        var sviKupci = kupacDatabase.getAll()
        for (k in sviKupci){
            kupacDatabase.delete(k)
        }
        startActivity(
            Intent(this@TrenutnoUTrgoviniActivity,
            AdminActivity::class.java)
        )
    }


}
