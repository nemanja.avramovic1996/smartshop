package hr.ferit.nemanjaavramovic.smartshop

import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import hr.ferit.nemanjaavramovic.smartshop.AdminActivities.AdminActivity
import hr.ferit.nemanjaavramovic.smartshop.Kupac.KupacDatabase
import hr.ferit.nemanjaavramovic.smartshop.KupacActivities.MainActivity

class LoginActivity : AppCompatActivity() {

    private val TAG = "LoginActivity"

    private var email: String? = null
    private var lozinka: String? = null

    private var elementZaboraviliSteLozinku: TextView? = null
    private var elementEmail: EditText? = null
    private var elementLozinka: EditText? = null
    private var buttonPrijava: Button? = null
    private var buttonRegistracija: Button? = null
    private var mProgressBar: ProgressDialog? = null
    var kupacDatabase = KupacDatabase.getInstance().kupacDao()

    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        setContentView(R.layout.activity_login)

        inicijalizacija()
    }

    private fun inicijalizacija() {
        elementZaboraviliSteLozinku = findViewById<View>(R.id.zaboravili_ste_lozinku) as TextView
        elementEmail = findViewById<View>(R.id.emailP) as EditText
        elementLozinka = findViewById<View>(R.id.lozinkaP) as EditText
        buttonPrijava = findViewById<View>(R.id.btn_prijavaP) as Button
        buttonRegistracija = findViewById<View>(R.id.btn_registracijaP) as Button
        mProgressBar = ProgressDialog(this)
        mAuth = FirebaseAuth.getInstance()
        elementZaboraviliSteLozinku!!
            .setOnClickListener { startActivity(
                Intent(this@LoginActivity,
                    ForgotPasswordActivity::class.java)
            ) }

        val num = kupacDatabase.getAll()
        var i = 0
        while(i < num.size){
            kupacDatabase.delete(num[i])
            i++
        }
        buttonRegistracija!!
            .setOnClickListener { startActivity(Intent(this@LoginActivity,
                CreateAccountActivity::class.java)) }
        buttonPrijava!!.setOnClickListener { prijavaKorisnika() }
    }

    private fun prijavaKorisnika() {
        email = elementEmail?.text.toString()
        lozinka = elementLozinka?.text.toString()
        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(lozinka)) {
            mProgressBar!!.setMessage("Registacija korisnika...")
            mProgressBar!!.show()
            Log.d(TAG, "Logging in user.")
            mAuth!!.signInWithEmailAndPassword(email!!, lozinka!!)
                .addOnCompleteListener(this) { task ->
                    mProgressBar!!.hide()
                    if (task.isSuccessful) {
                        Log.d(TAG, "Prijava s emailom:uspješna")
                        updateUI(email!!, lozinka!!)
                    } else {
                        Log.e(TAG, "Prijava s emailom:neuspješna", task.exception)
                        Toast.makeText(this@LoginActivity, "Autentifikacija nije uspješna.",
                            Toast.LENGTH_SHORT).show()
                    }
                }
        } else {
            Toast.makeText(this, "Unesite potrebne podatke", Toast.LENGTH_SHORT).show()
        }
    }

    private fun updateUI(email: String, lozinka: String) {
        if (email == "nemanja.crypto1996@gmail.com" && lozinka=="admin111111"){
            val intent = Intent(this@LoginActivity, AdminActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }
        else{
            val intent = Intent(this@LoginActivity, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }

    }

    override fun onBackPressed() {
        Toast.makeText(this@LoginActivity, "Ne mozete ici unazad",
            Toast.LENGTH_LONG).show()
    }
}
