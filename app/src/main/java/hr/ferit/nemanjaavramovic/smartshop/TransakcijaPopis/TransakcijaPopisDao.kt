package hr.ferit.nemanjaavramovic.smartshop.TransakcijaPopis

import androidx.room.*
@Dao
interface TransakcijaPopisDao{
    @Insert
    fun insert(transakcijaPopis: TransakcijaPopis);
    @Delete
    fun delete(transakcijaPopis: TransakcijaPopis);
    @Update
    fun update(transakcijaPopis: TransakcijaPopis);
    @Query("SELECT * FROM transakcije")
    fun getAll(): MutableList<TransakcijaPopis>;
    @Query("SELECT * FROM transakcije WHERE imeKupca = :imeKupca")
    fun getByName(imeKupca: String): TransakcijaPopis;
}