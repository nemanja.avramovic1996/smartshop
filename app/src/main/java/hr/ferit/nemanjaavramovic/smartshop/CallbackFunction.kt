package hr.ferit.nemanjaavramovic.smartshop

interface CallbackFunction {
    fun onCallback(flag: Int)
}