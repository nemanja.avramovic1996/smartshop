package hr.ferit.nemanjaavramovic.smartshop.KupacActivities

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton
import com.squareup.picasso.Picasso
import hr.ferit.nemanjaavramovic.smartshop.Proizvod.ProizvodKosarica
import hr.ferit.nemanjaavramovic.smartshop.Proizvod.ProizvodKosaricaDatabase
import hr.ferit.nemanjaavramovic.smartshop.R
import kotlinx.android.synthetic.main.activity_item.*

class ItemActivity : AppCompatActivity() {

    val proizvodKosaricaDatabase = ProizvodKosaricaDatabase.getInstance().proizvodKosaricaDao()

    companion object {
        const val PROIZVOD_IME: String = "ime"
        const val PROIZVOD_SLIKA: String = "slika"
        const val PROIZVOD_CIJENA: String = "cijena"
        lateinit var ime: TextView
        lateinit var cijena: TextView
        lateinit var slika: ImageView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        setContentView(R.layout.activity_item)
        inicijalizacija()
    }

    private fun inicijalizacija() {
        val imeProizvoda = intent?.getStringExtra(PROIZVOD_IME)
        val slikaProizvoda = intent?.getStringExtra(PROIZVOD_SLIKA)
        val cijenaProizvoda = intent?.getStringExtra(PROIZVOD_CIJENA)
        ime = findViewById(
            R.id.ime_proizvoda
        )
        cijena = findViewById(
            R.id.cijena_proizvoda
        )
        slika = findViewById(
            R.id.slika_proizvoda
        )
        if (imeProizvoda != null && slikaProizvoda != null && cijenaProizvoda != null) {
            postaviDetaljeProizvoda(imeProizvoda, slikaProizvoda, cijenaProizvoda)
        }
        val elegantNumber = findViewById<ElegantNumberButton>(R.id.kolicina_proizvoda)
        elegantNumber.setOnClickListener(ElegantNumberButton.OnClickListener {
        })
        dodajUKosaricuBtn.setOnClickListener {
            val num = proizvodKosaricaDatabase.getAll().size
            var i = 0
            var k = 0
            while( i < num){
                if (ime_proizvoda.text.toString() == proizvodKosaricaDatabase.getAll()[i].proizvodIme){
                    k++
                }
                i++
            }
            if(k != 0){
                Toast.makeText(this@ItemActivity, "Taj se proizvod već nalazi u košarici i ne možete ga ponovo dodati, ali količinu naknadno možete povećati u košarici",
                    Toast.LENGTH_LONG).show()
            }else{
                if (imeProizvoda != null && slikaProizvoda != null && cijenaProizvoda != null) {
                    dodajProizvod(imeProizvoda, slikaProizvoda, cijenaProizvoda)
                }
            }

        }

        odustaniBtn.setOnClickListener {odustaniOdProizvoda()}
    }

    private fun odustaniOdProizvoda() {
        this.finish()
    }

    private fun dodajProizvod(ime: String, slika: String, cijena: String) {
        val proizvod =
            ProizvodKosarica(
                0,
                ime,
                slika,
                (cijena_proizvoda.text.toString()).toDouble(),
                kolicina_proizvoda.number.toInt()
            )
        proizvodKosaricaDatabase.insert(proizvod)
        this.finish()


    }

    private fun postaviDetaljeProizvoda(ime: String, slika: String, cijena: String) {
        ime_proizvoda.text = ime
        var imView = findViewById<View>(R.id.slika_proizvoda) as ImageView
        var url = slika
        Picasso.get().load(url).into(imView)
        cijena_proizvoda.text = cijena
    }
}
