@file:Suppress("DEPRECATION")

package hr.ferit.nemanjaavramovic.smartshop

import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase



//globalne varijable za autentifikaciju
private var ime: String? = null
private var prezime: String? = null
private var racun: String? = null
private var email: String? = null
private var lozinka: String? = null
private var lozinkaPotvrda: String? = null

class CreateAccountActivity : AppCompatActivity() {

    private var elementIme: EditText? = null
    private var elementPrezime: EditText? = null
    private var elementRacun: EditText? = null
    private var elementEmail: EditText? = null
    private var elementLozinka: EditText? = null
    private var elementLozinkaPotvrda: EditText? = null
    private var buttonRegistracija: Button? = null
    private var mProgressBar: ProgressDialog? = null

    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null
    private var mAuth: FirebaseAuth? = null

    private val TAG = "CreateAccountActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        setContentView(R.layout.activity_create_account)

        inicijalizacija()
    }

    private fun inicijalizacija() {
        elementIme = findViewById<View>(R.id.ime) as EditText
        elementPrezime = findViewById<View>(R.id.prezime) as EditText
        elementRacun = findViewById<View>(R.id.racun) as EditText
        elementEmail = findViewById<View>(R.id.email) as EditText
        elementLozinka = findViewById<View>(R.id.lozinka) as EditText
        elementLozinkaPotvrda = findViewById<View>(R.id.lozinkaPotvrda) as EditText
        buttonRegistracija = findViewById<View>(R.id.btn_registracija) as Button
        mProgressBar = ProgressDialog(this)
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference.child("Korisnici")
        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance()
        buttonRegistracija!!.setOnClickListener { kreirajRacun() }
    }

    private fun kreirajRacun() {
        ime = elementIme?.text.toString()
        prezime = elementPrezime?.text.toString()
        racun = elementRacun?.text.toString()
        email = elementEmail?.text.toString()
        lozinka = elementLozinka?.text.toString()
        lozinkaPotvrda = elementLozinkaPotvrda?.text.toString()
        if(lozinka != lozinkaPotvrda){
            Toast.makeText(this, "Potvrdite lozinku", Toast.LENGTH_SHORT).show()
        }
        else{
            if (!TextUtils.isEmpty(ime) && !TextUtils.isEmpty(prezime)
                && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(lozinka) && !TextUtils.isEmpty(racun) && !TextUtils.isEmpty(lozinkaPotvrda)) {
                    mProgressBar!!.setMessage("Registracija korisnika...")
                    mProgressBar!!.show()

                    mAuth!!
                        .createUserWithEmailAndPassword(email!!, lozinka!!)
                        .addOnCompleteListener(this) { task ->
                            mProgressBar!!.hide()
                            if (task.isSuccessful) {
                                //Prijava uspješna, update UI podacima prijavljenog korisnika
                                Log.d(TAG, "createUserWithEmail:success")
                                val korisnikID = mAuth!!.currentUser!!.uid
                                //Verifikacija emaila
                                verifikacijaEmaila();
                                //Update profil korisnikovim informacijama
                                val trenutniKorisnikDb = mDatabaseReference!!.child(korisnikID)
                                trenutniKorisnikDb.child("ime").setValue(ime)
                                trenutniKorisnikDb.child("prezime").setValue(prezime)
                                trenutniKorisnikDb.child("racun").setValue(racun)
                                updateKorisnikoveInformacijeUI()
                            } else {
                                //Ako prijava nije uspješna
                                Log.w(TAG, "createUserWithEmail:failure", task.exception)
                                Toast.makeText(this@CreateAccountActivity, "Registracija nije uspjela.",
                                    Toast.LENGTH_SHORT).show()
                            }
                        }


                } else {
                Toast.makeText(this, "Popunite sva tražena polja", Toast.LENGTH_SHORT).show()
            }
        }


    }



    private fun verifikacijaEmaila() {
        val mUser = mAuth!!.currentUser;
        mUser!!.sendEmailVerification()
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Toast.makeText(this@CreateAccountActivity,
                        "Verifikacijski email poslan na " + mUser.getEmail(),
                        Toast.LENGTH_SHORT).show()
                } else {
                    Log.e(TAG, "sendEmailVerification", task.exception)
                    Toast.makeText(this@CreateAccountActivity,
                        "Slanje verifikacijskog emaila nije uspjelo.",
                        Toast.LENGTH_SHORT).show()
                }
            }
    }

    private fun updateKorisnikoveInformacijeUI() {
        val intent = Intent(this@CreateAccountActivity, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

}
