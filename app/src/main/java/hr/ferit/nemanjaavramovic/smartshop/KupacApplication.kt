package hr.ferit.nemanjaavramovic.smartshop

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context

@SuppressLint("Registered")

class KupacApplication : ProizvodKosaricaApplication()  {
    companion object {
        lateinit var KupacApplicationContext: Context
    }
    override fun onCreate() {
        super.onCreate()
        KupacApplicationContext = this
    }
}