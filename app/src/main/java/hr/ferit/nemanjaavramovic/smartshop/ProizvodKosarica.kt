package hr.ferit.nemanjaavramovic.smartshop


import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "proizvodi")
data class ProizvodKosarica(
    @PrimaryKey(autoGenerate = true) var id: Int,
    @ColumnInfo(name = "proizvodIme") var proizvodIme: String,
    @ColumnInfo(name = "proizvodSlika") var proizvodSlika: String,
    @ColumnInfo(name = "proizvodCijena") var proizvodCijena: Double,
    @ColumnInfo(name = "proizvodKolicina") var proizvodKolicina: Int
)