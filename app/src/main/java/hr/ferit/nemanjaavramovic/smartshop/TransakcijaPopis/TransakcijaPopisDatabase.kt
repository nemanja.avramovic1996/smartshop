package hr.ferit.nemanjaavramovic.smartshop.TransakcijaPopis

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import hr.ferit.nemanjaavramovic.smartshop.MyApplication


@Database(version = 4, entities = arrayOf(TransakcijaPopis::class))
abstract class TransakcijaPopisDatabase : RoomDatabase() {
    abstract fun transakcijaPopisDao(): TransakcijaPopisDao
    companion object {
        private const val NAME = "transakcije_database"
        private var INSTANCE: TransakcijaPopisDatabase? = null
        fun getInstance(): TransakcijaPopisDatabase {
            if(INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    MyApplication.ApplicationContext,
                    TransakcijaPopisDatabase::class.java,
                    NAME
                )
                    .allowMainThreadQueries()
                    .build()
            }
            return INSTANCE as TransakcijaPopisDatabase
        }
    }
}