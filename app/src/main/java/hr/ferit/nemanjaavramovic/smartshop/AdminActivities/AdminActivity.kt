package hr.ferit.nemanjaavramovic.smartshop.AdminActivities

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import hr.ferit.nemanjaavramovic.smartshop.Kupac.Kupac
import hr.ferit.nemanjaavramovic.smartshop.Kupac.KupacDatabase
import hr.ferit.nemanjaavramovic.smartshop.LoginActivity
import hr.ferit.nemanjaavramovic.smartshop.R
import hr.ferit.nemanjaavramovic.smartshop.SviProizvodi.SviProizvodi
import hr.ferit.nemanjaavramovic.smartshop.SviProizvodi.SviProizvodiDatabase
import hr.ferit.nemanjaavramovic.smartshop.TransakcijaPopis.TransakcijaPopis
import hr.ferit.nemanjaavramovic.smartshop.TransakcijaPopis.TransakcijaPopisDatabase
import kotlinx.android.synthetic.main.activity_admin.*

class AdminActivity : AppCompatActivity() {
    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabaseReference2: DatabaseReference? = null
    private var mDatabaseReference3: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null
    private var mAuth: FirebaseAuth? = null

    var kupacDatabase = KupacDatabase.getInstance().kupacDao()
    var transakcijeDatabase = TransakcijaPopisDatabase.getInstance().transakcijaPopisDao()
    var sviProizvodiDatabase = SviProizvodiDatabase.getInstance().sviProizvodiDao()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        setContentView(R.layout.activity_admin)
        inicijalizacija()

    }



    @SuppressLint("WrongConstant")
    private fun inicijalizacija() {

        TrenutnoUTrgoviniActivity.kupacDatabase = KupacDatabase.getInstance().kupacDao()
        PovijestTransakcijaActivity.transakcijeDatabase = TransakcijaPopisDatabase.getInstance().transakcijaPopisDao()
        SviProizvodiActivity.sviProizvodiDatabase = SviProizvodiDatabase.getInstance().sviProizvodiDao()

        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference.child("Korisnici")
        mDatabaseReference2 = mDatabase!!.reference.child("Transakcije")
        mDatabaseReference3 = mDatabase!!.reference.child("Proizvodi")
        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance()

        tv_stanje.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
                omoguciButtone()
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
            }

            override fun afterTextChanged(s: Editable) {}
        })




    }

    private fun omoguciButtone(){
        adminOdjavaBtn.setOnClickListener { odjaviteSe() }
        trenutnoUTrgoviniBtn.setOnClickListener { pogledajTkoJeUTrgovini() }
        dodajProizvodBtn.setOnClickListener { dodajNoviProizvod() }
        povijestTransakcijaBtn.setOnClickListener { otvoriPovijestTransakcija() }
        sviProizvodiBtn.setOnClickListener{ pogledajSveProizvode() }
    }

    private fun pogledajSveProizvode() {
        startActivity(Intent(this@AdminActivity,
            SviProizvodiActivity::class.java))
    }

    private fun otvoriPovijestTransakcija() {
        startActivity(Intent(this@AdminActivity,
            PovijestTransakcijaActivity::class.java))
    }

    private fun dodajNoviProizvod() {
        startActivity(Intent(this@AdminActivity,
            DodajNoviProizvodActivity::class.java))
    }



    private fun pogledajTkoJeUTrgovini() {
        startActivity(Intent(this@AdminActivity,
            TrenutnoUTrgoviniActivity::class.java))
    }





    private fun odjaviteSe(){

        izbrisiBazuKupci()
        mAuth?.signOut()
        startActivity(
            Intent(this@AdminActivity,
                LoginActivity::class.java)
        )
    }




    override fun onStart() {
        super.onStart()
        val mUser = mAuth!!.currentUser
        val mUserReference = mDatabaseReference!!.child(mUser!!.uid)
        Toast.makeText(this, "Baze ažurirane", Toast.LENGTH_LONG).show()
        //tvEmail!!.text = mUser.email
        mUserReference.addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {
                tv_stanje.text = snapshot.child("racun").value as String + " kn"
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        })
        izbrisiBazuKupci()
        popuniBazuKupci()

        izbrisiBazuTransakcije()
        popuniBazuTransakcije()

        izbrisiBazuSviProizvodi()
        popuniBazuSviProizvodi()

    }

    private fun popuniBazuSviProizvodi() {
        mDatabaseReference3?.addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {
                for(k in snapshot.children){
                    val idProizvoda = k.key
                    val imeProizvoda = k.child("imeProizvoda").value as String
                    val slikaProizvoda = k.child("slikaProizvoda").value as String
                    val cijenaProizvoda = k.child("cijenaProizvoda").value as Double
                    val sviProizvodi = idProizvoda?.let {
                        SviProizvodi(
                            0,
                            it,
                            imeProizvoda,
                            slikaProizvoda,
                            cijenaProizvoda.toDouble()
                        )
                    }
                    if (sviProizvodi != null) {
                        sviProizvodiDatabase.insert(sviProizvodi)
                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    private fun izbrisiBazuSviProizvodi() {
        val sviProizvodi = sviProizvodiDatabase.getAll()
        if (sviProizvodi.size > 0){
            for (k in sviProizvodi){
                sviProizvodiDatabase.delete(k)
            }
        }
    }


    private fun popuniBazuTransakcije() {

        mDatabaseReference2?.addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {
                for(k in snapshot.children){
                    val imeKupca = k.child("imeKupca").value as String
                    val prezimeKupca = k.child("prezimeKupca").value as String
                    val ukupnaCijena = k.child("ukupnaCijena").value as Double
                    val datum = k.child("datum").value as String
                    val vrijeme = k.child("vrijeme").value as String
                    val transakcija = TransakcijaPopis(
                        0,
                        imeKupca,
                        prezimeKupca,
                        ukupnaCijena.toString(),
                        datum,
                        vrijeme
                    )
                    transakcijeDatabase.insert(transakcija)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    private fun izbrisiBazuTransakcije() {
        val sveTransakcije = transakcijeDatabase.getAll()
        if (sveTransakcije.size > 0){
            for (k in sveTransakcije){
                transakcijeDatabase.delete(k)
            }
        }
    }

    private fun izbrisiBazuKupci() {
        val sviKupci = kupacDatabase.getAll()
        if (sviKupci.size > 0){
            for (k in sviKupci){
                kupacDatabase.delete(k)
            }
        }
    }

    private fun popuniBazuKupci() {
        mDatabaseReference?.addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {
                for(k in snapshot.children){
                    if(k.child("trenutno_u_trgovini").value =="+" ){
                        val kupacIme = k.child("ime").value as String
                        val kupacPrezime = k.child("prezime").value as String
                        val kupac =
                            Kupac(
                                0,
                                kupacIme,
                                kupacPrezime
                            )
                        kupacDatabase.insert(kupac)
                    }

                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    override fun onBackPressed() {
        Toast.makeText(this, "Ne mozete ici unazad, prvo se morate odjaviti",
            Toast.LENGTH_LONG).show()
    }



}
