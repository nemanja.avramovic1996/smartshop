package hr.ferit.nemanjaavramovic.smartshop.Proizvod

import com.google.firebase.database.Exclude

data class Proizvod(
    val imeProizvoda: String,
    val slikaProizvoda: String,
    val cijenaProizvoda: Double
    ){

    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "imeProizvoda" to imeProizvoda,
            "slikaProizvoda" to slikaProizvoda,
            "cijenaProizvoda" to cijenaProizvoda
        )
    }
}