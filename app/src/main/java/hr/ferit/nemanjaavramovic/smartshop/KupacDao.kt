package hr.ferit.nemanjaavramovic.smartshop

import androidx.room.*

@Dao
interface KupacDao{
    @Insert
    fun insert(kupac: Kupac);
    @Delete
    fun delete(kupac: Kupac);
    @Update
    fun update(kupac: Kupac);
    @Query("SELECT * FROM kupci")
    fun getAll(): MutableList<Kupac>;
    @Query("SELECT * FROM kupci WHERE kupacIme = :kupacIme")
    fun getByName(kupacIme: String): Kupac;
}