package hr.ferit.nemanjaavramovic.smartshop

object ProizvodRepozitorij{
    val proizvodi: MutableList<Proizvod>

    init {
        proizvodi = retrieveUnits()
    }

    private fun retrieveUnits(): MutableList<Proizvod> {
        return mutableListOf(
            Proizvod(0,"COCA_COLA","SDA",5.99),
            Proizvod(1,"MARS","1",4.99)

        )
    }

    /*fun remove(id: Int) = books.removeAll{ book -> book.id == id }*/
    fun get(id: Int): Proizvod? = proizvodi.find { proizvod -> proizvod.id == id }
    fun add(unit: Proizvod) = proizvodi.add(unit)
}