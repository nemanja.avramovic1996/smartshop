package hr.ferit.nemanjaavramovic.smartshop.SviProizvodi

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import hr.ferit.nemanjaavramovic.smartshop.R
import kotlinx.android.synthetic.main.svi_proizvodi_layout.view.*


class SviProizvodiAdapter(sviProizvodi: MutableList<SviProizvodi>,
                              sviProizvodiListener: SviProizvodiInteractionListener
) : RecyclerView.Adapter<SviProizvodiAdapter.SviProizvodiHolder>(){


    private val sviProizvodi: MutableList<SviProizvodi>
    private val sviProizvodiListener: SviProizvodiInteractionListener

    init {
        this.sviProizvodi = mutableListOf()
        this.sviProizvodi.addAll(sviProizvodi)
        this.sviProizvodiListener = sviProizvodiListener
    }


    fun refreshData(sviProizvodi: MutableList<SviProizvodi>) {
        this.sviProizvodi.clear()
        this.sviProizvodi.addAll(sviProizvodi)
        this.notifyDataSetChanged()
    }

    override fun getItemCount(): Int = sviProizvodi.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SviProizvodiHolder {
        val sviProizvodiView = LayoutInflater.from(parent.context).inflate(R.layout.svi_proizvodi_layout, parent, false)
        return SviProizvodiHolder(
            sviProizvodiView
        )
    }


    override fun onBindViewHolder(holder: SviProizvodiHolder, position: Int) {
        val sviProizvodi = sviProizvodi[position]
        holder.bind(sviProizvodi, sviProizvodiListener)
    }


    class SviProizvodiHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(sviProizvodi: SviProizvodi, sviProizvodiListener: SviProizvodiInteractionListener) {
            itemView.sviProizvodiIme.text = sviProizvodi.imeProizvoda
            Picasso.get().load(sviProizvodi.slikaProizvoda).into(itemView.sviProizvodiSlika)
            itemView.sviProizvodiCijena.text="Cijena: " + sviProizvodi.cijenaProizvoda.toString() +"kn"
            itemView.svi_proizvodi_layout.setOnClickListener{sviProizvodiListener.onUpdate(sviProizvodi.idProizvoda, sviProizvodi.imeProizvoda, sviProizvodi.slikaProizvoda,sviProizvodi.cijenaProizvoda)}
        }
    }
}
