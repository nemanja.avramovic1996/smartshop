package hr.ferit.nemanjaavramovic.smartshop.AdminActivities

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import hr.ferit.nemanjaavramovic.smartshop.R
import hr.ferit.nemanjaavramovic.smartshop.SviProizvodi.SviProizvodiAdapter
import hr.ferit.nemanjaavramovic.smartshop.SviProizvodi.SviProizvodiDao
import hr.ferit.nemanjaavramovic.smartshop.SviProizvodi.SviProizvodiInteractionListener
import kotlinx.android.synthetic.main.activity_svi_proizvodi.*

class SviProizvodiActivity : AppCompatActivity() {


    companion object {
        lateinit var sviProizvodiDatabase: SviProizvodiDao
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        setContentView(R.layout.activity_svi_proizvodi)

        inicijalizacija()
        prikaziSveProizvode()
    }

    @SuppressLint("WrongConstant")
    private fun inicijalizacija() {
        recycler_view_svi_proizvodi.layoutManager = LinearLayoutManager(this,
            LinearLayout.VERTICAL,false)
    }


    private fun prikaziSveProizvode() {
        val sviProizvodiListener = object:
            SviProizvodiInteractionListener {
            override fun onUpdate(
                idProizvoda: String,
                imeProizvoda: String,
                slikaProizvoda: String,
                cijenaProizvoda: Double
            ) {
                otvoriUpdateProizvod(idProizvoda, imeProizvoda, slikaProizvoda, cijenaProizvoda)
            }
        }

        recycler_view_svi_proizvodi.adapter=
            SviProizvodiAdapter(
                sviProizvodiDatabase.getAll(),
                sviProizvodiListener
            )
    }


    private fun otvoriUpdateProizvod(
        idProizvoda: String,
        imeProizvoda: String,
        slikaProizvoda: String,
        cijenaProizvoda: Double
    ) {
        val displayIntent = Intent(this@SviProizvodiActivity, ProizvodUpdateActivity::class.java)
        displayIntent.putExtra(ProizvodUpdateActivity.PROIZVOD_ID, idProizvoda)
        displayIntent.putExtra(ProizvodUpdateActivity.PROIZVOD_IME, imeProizvoda)
        displayIntent.putExtra(ProizvodUpdateActivity.PROIZVOD_SLIKA, slikaProizvoda)
        displayIntent.putExtra(ProizvodUpdateActivity.PROIZVOD_CIJENA, cijenaProizvoda)
        startActivity(displayIntent)
    }
}
