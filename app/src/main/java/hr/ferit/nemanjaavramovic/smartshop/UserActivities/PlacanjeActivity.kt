package hr.ferit.nemanjaavramovic.smartshop.UserActivities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import hr.ferit.nemanjaavramovic.smartshop.Proizvod.ProizvodKosaricaDatabase
import hr.ferit.nemanjaavramovic.smartshop.R
import kotlinx.android.synthetic.main.activity_placanje.*
import java.math.RoundingMode

class PlacanjeActivity : AppCompatActivity() {

    companion object {
        const val UKUPNA_CIJENA: String = "ukupna_cijena"
    }
    val proizvodKosaricaDatabase = ProizvodKosaricaDatabase.getInstance().proizvodKosaricaDao()

    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null
    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_placanje)
        inicijalizacija()
    }

    private fun inicijalizacija() {



        val ukupnaCijena = intent?.getDoubleExtra(UKUPNA_CIJENA.toString(), 0.0)
        val upit = findViewById<TextView>(R.id.upitTxt)
        upit.text = "Da li ste sigurni da želite završiti s kupovinom i platiti " + ukupnaCijena.toString() + "kn?"

        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("Korisnici")
        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance()

        var mUser = mAuth!!.currentUser
        var mUserReference = mDatabaseReference!!.child(mUser!!.uid)
        mUserReference.addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {
                trenutnoStanje.text = snapshot.child("racun").value as String
                stanjeTxt.text = "Trenutno stanje na vašem računu je " + trenutnoStanje.text + "kn"

            }
            override fun onCancelled(databaseError: DatabaseError) {}

        })

        var mAdminReference = mDatabaseReference!!.child("1eqTJGacDdQMXV0Eo590xa2t6cz2")
        mAdminReference.addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {
                trenutnoStanjeadmin.text = snapshot.child("racun").value as String

            }
            override fun onCancelled(databaseError: DatabaseError) {}

        })




        val plati = findViewById<Button>(R.id.zavrsi_placanjeBtn)
        plati.setOnClickListener{zavrsiPlacanje(ukupnaCijena)}
    }

    private fun zavrsiPlacanje(ukupnaCijena: Double?) {

        if (trenutnoStanje.text.toString().toDouble() < ukupnaCijena!!){
            Toast.makeText(this@PlacanjeActivity, "Nemate dovoljno novca na računu za kupovinu",
                Toast.LENGTH_LONG).show()
        }
        else{
            var novoStanje : Double = 0.0
            var novoStanjeAdmin : Double = 0.0

            novoStanje = trenutnoStanje.text.toString().toDouble() - ukupnaCijena!!
            novoStanje = novoStanje.toBigDecimal().setScale(2, RoundingMode.UP).toDouble()

            novoStanjeAdmin = trenutnoStanjeadmin.text.toString().toDouble() + ukupnaCijena!!
            novoStanjeAdmin = novoStanjeAdmin.toBigDecimal().setScale(2, RoundingMode.UP).toDouble()

            val korisnikID = mAuth!!.currentUser!!.uid
            val trenutniKorisnikDb = mDatabaseReference!!.child(korisnikID)
            val adminKorisnikDb = mDatabaseReference!!.child("1eqTJGacDdQMXV0Eo590xa2t6cz2")

            trenutniKorisnikDb.child("racun").setValue(novoStanje.toString())
            adminKorisnikDb.child("racun").setValue(novoStanjeAdmin.toString())

            val num = proizvodKosaricaDatabase.getAll()
            var i = 0
            while(i < num.size){
                proizvodKosaricaDatabase.delete(num[i])
                i++
            }

            Toast.makeText(this@PlacanjeActivity, "Plaćanje uspješno, možete izaći iz trgovine",
                Toast.LENGTH_LONG).show()

            val displayIntent = Intent(this, MainActivity::class.java)
            startActivity(displayIntent)
        }



    }
}
