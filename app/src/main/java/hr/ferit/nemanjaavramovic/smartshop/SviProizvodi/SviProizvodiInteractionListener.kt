package hr.ferit.nemanjaavramovic.smartshop.SviProizvodi

interface SviProizvodiInteractionListener {
    //fun onDelete(idProizvoda: Int)
    fun onUpdate(idProizvoda: String, imeProizvoda: String, slikaProizvoda: String, cijenaProizvoda: Double)
}