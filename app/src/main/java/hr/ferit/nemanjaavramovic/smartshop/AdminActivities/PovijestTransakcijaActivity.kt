package hr.ferit.nemanjaavramovic.smartshop.AdminActivities

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import hr.ferit.nemanjaavramovic.smartshop.R
import hr.ferit.nemanjaavramovic.smartshop.TransakcijaPopis.TransakcijaPopisAdapter
import hr.ferit.nemanjaavramovic.smartshop.TransakcijaPopis.TransakcijaPopisDao
import hr.ferit.nemanjaavramovic.smartshop.TransakcijaPopis.TransakcijaPopisInteractionListener
import kotlinx.android.synthetic.main.activity_povijest_transakcija.*

class PovijestTransakcijaActivity : AppCompatActivity() {


    companion object {
        lateinit var transakcijeDatabase: TransakcijaPopisDao
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        setContentView(R.layout.activity_povijest_transakcija)

        inicijalizacija()
        prikaziTransakcije()
    }


    @SuppressLint("WrongConstant")
    private fun inicijalizacija() {

        recycler_view_transakcije.layoutManager = LinearLayoutManager(this,
            LinearLayout.VERTICAL,false)

    }

    private fun prikaziTransakcije() {
        val transakcijeListener = object:
            TransakcijaPopisInteractionListener {
        }

        recycler_view_transakcije.adapter=
            TransakcijaPopisAdapter(
                transakcijeDatabase.getAll(),
                transakcijeListener
            )
    }
}
