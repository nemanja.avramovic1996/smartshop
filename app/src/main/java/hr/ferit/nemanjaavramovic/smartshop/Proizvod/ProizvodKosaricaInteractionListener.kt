package hr.ferit.nemanjaavramovic.smartshop.Proizvod


interface ProizvodKosaricaInteractionListener {
    fun onDelete(proizvodIme: String)
    fun onUpdate(proizvodIme: String, number: String)
}