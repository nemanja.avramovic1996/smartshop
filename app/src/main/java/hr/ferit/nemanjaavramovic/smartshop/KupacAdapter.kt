package hr.ferit.nemanjaavramovic.smartshop

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton
import kotlinx.android.synthetic.main.kupac_layout.view.*

class KupacAdapter(kupci: MutableList<Kupac>,
                              kupciListener: KupacInteractionListener
) : RecyclerView.Adapter<KupacAdapter.KupciHolder>(){


    private val kupci: MutableList<Kupac>
    private val kupciListener: KupacInteractionListener

    init {
        this.kupci = mutableListOf()
        this.kupci.addAll(kupci)
        this.kupciListener = kupciListener
    }


    fun refreshData(kupci: MutableList<Kupac>) {
        this.kupci.clear()
        this.kupci.addAll(kupci)
        this.notifyDataSetChanged()
    }

    override fun getItemCount(): Int = kupci.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KupciHolder {
        val kupciView = LayoutInflater.from(parent.context).inflate(R.layout.kupac_layout, parent, false)
        return KupciHolder(kupciView)
    }


    override fun onBindViewHolder(holder: KupciHolder, position: Int) {
        val kupac = kupci[position]
        /*val onePrice : Double = proizvod.proizvodCijena * proizvod.proizvodKolicina
        totalPrice = totalPrice + onePrice*/
        holder.bind(kupac, kupciListener)
    }


    class KupciHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(kupac: Kupac, kupciListener: KupacInteractionListener) {
            itemView.kupacIme.text = kupac.kupacIme
            itemView.kupacPrezime.text = kupac.kupacPrezime
        }
    }
}