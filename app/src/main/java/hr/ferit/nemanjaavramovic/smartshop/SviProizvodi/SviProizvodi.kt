package hr.ferit.nemanjaavramovic.smartshop.SviProizvodi

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "sviProizvodi")
data class SviProizvodi(
    @PrimaryKey(autoGenerate = true) var id: Int,
    @ColumnInfo(name = "idProizvoda") var idProizvoda: String,
    @ColumnInfo(name = "imeProizvoda") var imeProizvoda: String,
    @ColumnInfo(name = "slikaProizvoda") var slikaProizvoda: String,
    @ColumnInfo(name = "cijenaProizvoda") var cijenaProizvoda: Double
)