package hr.ferit.nemanjaavramovic.smartshop



import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(version = 4, entities = arrayOf(ProizvodKosarica::class))
abstract class ProizvodKosaricaDatabase : RoomDatabase() {
    abstract fun proizvodKosaricaDao(): ProizvodKosaricaDao
    companion object {
        private const val NAME = "proizvod_kosarica_database"
        private var INSTANCE: ProizvodKosaricaDatabase? = null
        fun getInstance(): ProizvodKosaricaDatabase {
            if(INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                        ProizvodKosaricaApplication.ApplicationContext,
                        ProizvodKosaricaDatabase::class.java,
                        NAME)
                    .allowMainThreadQueries()
                    .build()
            }
            return INSTANCE as ProizvodKosaricaDatabase
        }
    }
}