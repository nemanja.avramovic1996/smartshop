package hr.ferit.nemanjaavramovic.smartshop.AdminActivities

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.FirebaseApp
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import hr.ferit.nemanjaavramovic.smartshop.Proizvod.Proizvod
import hr.ferit.nemanjaavramovic.smartshop.R
import kotlinx.android.synthetic.main.activity_dodaj_novi_proizvod.*
import java.math.RoundingMode

class DodajNoviProizvodActivity : AppCompatActivity() {


    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null
    private lateinit var imeProizvoda: EditText
    private lateinit var slikaProizvoda: EditText
    private lateinit var cijenaProizvoda: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        setContentView(R.layout.activity_dodaj_novi_proizvod)

        inicijalizacija()

    }

    private fun inicijalizacija() {
        imeProizvoda = findViewById<View>(R.id.imeProizvoda) as EditText
        slikaProizvoda = findViewById<View>(R.id.slikaProizvoda) as EditText
        cijenaProizvoda = findViewById<View>(R.id.cijenaProizvoda) as EditText

        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference
        FirebaseApp.initializeApp(this)





        dodajBtn.setOnClickListener{dodajProizvod()}
    }



    private fun dodajProizvod() {
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference
        FirebaseApp.initializeApp(this);

        if (!TextUtils.isEmpty(imeProizvoda.text.toString())
            && !TextUtils.isEmpty(slikaProizvoda.text.toString()) && !TextUtils.isEmpty(cijenaProizvoda.text.toString())) {
            if(cijenaProizvoda.text.toString().toBigDecimal().setScale(2, RoundingMode.UP)
                    .toDouble() <= 0.0){
                Toast.makeText(this, "Cijena ne može biti 0", Toast.LENGTH_SHORT).show()
            }else{
                val data_proizvod = mDatabaseReference!!.child("Proizvodi").push()
                var cijenaC :Double = 0.0

                cijenaC = cijenaProizvoda.text.toString().toBigDecimal().setScale(2, RoundingMode.UP).toDouble()

                if (cijenaC > 0 && cijenaC < 1){
                    cijenaC = cijenaC
                }

                else if (cijenaC % cijenaC.toInt() == 0.0){
                    cijenaC = cijenaC - 0.01

                }else{
                    cijenaC = cijenaProizvoda.text.toString().toBigDecimal().setScale(2, RoundingMode.UP)
                        .toDouble()
                }
                val proizvod =
                    Proizvod(
                        imeProizvoda.text.toString(),
                        slikaProizvoda.text.toString(),
                        cijenaC
                    )

                val map_proizvod = proizvod.toMap()
                Toast.makeText(this, "Dodan je novi proizvod", Toast.LENGTH_SHORT).show()
                data_proizvod.setValue(map_proizvod)

                

                startActivity(Intent(this,
                    AdminActivity::class.java))





            }


        }else {
            Toast.makeText(this, "Popunite sva tražena polja", Toast.LENGTH_SHORT).show()
        }

    }


}
