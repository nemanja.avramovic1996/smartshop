package hr.ferit.nemanjaavramovic.smartshop.SviProizvodi

import androidx.room.*

@Dao
interface SviProizvodiDao{
    @Insert
    fun insert(sviProizvodi: SviProizvodi);
    @Delete
    fun delete(sviProizvodi: SviProizvodi);
    @Update
    fun update(sviProizvodi: SviProizvodi);
    @Query("SELECT * FROM sviProizvodi")
    fun getAll(): MutableList<SviProizvodi>;
    @Query("SELECT * FROM sviProizvodi WHERE imeProizvoda = :imeProizvoda")
    fun getByName(imeProizvoda: String): SviProizvodi;
}