package hr.ferit.nemanjaavramovic.smartshop


import android.annotation.SuppressLint
import android.app.Application
import android.content.Context

@SuppressLint("Registered")

open class MyApplication : Application() {
    companion object {
        lateinit var ApplicationContext: Context
            private set
    }
    override fun onCreate() {
        super.onCreate()
        ApplicationContext = this
    }
}