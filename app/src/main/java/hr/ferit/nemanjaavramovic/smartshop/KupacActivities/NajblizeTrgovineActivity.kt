package hr.ferit.nemanjaavramovic.smartshop.KupacActivities


import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.ActivityInfo
import android.location.*
import android.media.SoundPool
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import hr.ferit.nemanjaavramovic.smartshop.R
import hr.ferit.nemanjaavramovic.smartshop.hasPermissionCompat
import hr.ferit.nemanjaavramovic.smartshop.requestPermisionCompat
import java.io.IOException
import java.util.*

class NajblizeTrgovineActivity : AppCompatActivity(),OnMapReadyCallback {

    private lateinit var mSoundPool: SoundPool
    private var mLoaded: Boolean = false
    var mSoundMap: HashMap<Int, Int> = HashMap()

    private val PERMISSION_CODE = 1000
    private val IMAGE_CAPTURE_CODE = 1001
    var image_uri: Uri? = null
    lateinit var nearadr: String
    lateinit var  photoPath: String
    val REQUEST_TAKE_PHOTO = 1
    lateinit var mapFragment: SupportMapFragment
    lateinit var googleMap: GoogleMap
    private val locationPermission = Manifest.permission.ACCESS_FINE_LOCATION
    private val locationRequestCode = 10
    private lateinit var  locationManager: LocationManager
    var Check = 0


    private val locationListener = object: LocationListener {
        override fun onProviderEnabled(provider: String?) { }

        override fun onProviderDisabled(provider: String?) { }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) { }

        override fun onLocationChanged(location: Location?) {
            updateLocationDisplay(location)
        }
    }

    private fun updateLocationDisplay(location: Location?) {
        val lat = location?.latitude ?: 0.0
        val lon = location?.longitude ?: 0.0
        var myLocation = LatLng(lat,lon)

        googleMap.addMarker(MarkerOptions().position(myLocation).title("Vaša lokacija").icon(
            BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)
        ))

        //googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation,10.2f))
        if(location != null && Geocoder.isPresent()) {
            val geocoder = Geocoder(this, Locale.getDefault());
            try {
                val nearByAddresses = geocoder.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    1
                );

                if (nearByAddresses.size > 0) {
                    val stringBuilder = StringBuilder();
                    val nearestAddress = nearByAddresses[0];
                    stringBuilder.append("\nUlica: ")
                        .append(nearestAddress.getAddressLine(0))
                        .append("\nMjesto: ")
                        .append(nearestAddress.getLocality())
                        .append("\nDržava: ")
                        .append(nearestAddress.getCountryName());
                    nearadr=nearestAddress.locality
                }
            } catch (e: IOException) {
                e.printStackTrace(); }
        }
    }



    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        setContentView(R.layout.activity_najblize_trgovine)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.GoogleMapsFragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        trackLocation()

    }






    private fun trackLocation() {
        if(hasPermissionCompat(locationPermission)){
            startTrackingLocation()
        } else {
            requestPermisionCompat(arrayOf(locationPermission), locationRequestCode)
        }
    }


    private fun startTrackingLocation() {
        Log.d("TAG", "Tracking location")

        val criteria: Criteria = Criteria()
        criteria.accuracy = Criteria.ACCURACY_FINE



        val provider = locationManager.getBestProvider(criteria, true)
        val minTime = 1000L
        val minDistance = 10.0F
        try{
            locationManager.requestLocationUpdates(provider, minTime, minDistance, locationListener)
        } catch (e: SecurityException){
            Toast.makeText(this,
                R.string.permissionNotGranted, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onPause() {
        super.onPause()
        locationManager.removeUpdates(locationListener)
    }


    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap =googleMap

        val FERITShop = LatLng(45.54, 18.69)
        val FERShop = LatLng(45.8, 15.97)
        val FESBShop = LatLng(43.5, 16.46)
        val FOIShop = LatLng(46.3, 16.33)
        val RiTehShop = LatLng(45.32, 14.46)
        val Croatia = LatLng(45.33, 16.37)


        googleMap.addMarker(MarkerOptions().position(FERITShop).title("FERIT SmartShop"))
        googleMap.addMarker(MarkerOptions().position(FERShop).title("FER SmartShop"))
        googleMap.addMarker(MarkerOptions().position(FESBShop).title("FESB SmartShop"))
        googleMap.addMarker(MarkerOptions().position(FOIShop).title("FOI SmartShop"))
        googleMap.addMarker(MarkerOptions().position(RiTehShop).title("RiTeh SmartShop"))

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Croatia, 6F),3000,null)
        googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL
        googleMap.uiSettings.isZoomControlsEnabled = true
    }


}

