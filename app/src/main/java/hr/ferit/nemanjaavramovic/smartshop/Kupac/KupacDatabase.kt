package hr.ferit.nemanjaavramovic.smartshop.Kupac



import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import hr.ferit.nemanjaavramovic.smartshop.MyApplication

@Database(version = 4, entities = arrayOf(Kupac::class))
abstract class KupacDatabase : RoomDatabase() {
    abstract fun kupacDao(): KupacDao
    companion object {
        private const val NAME = "kupac_database"
        private var INSTANCE: KupacDatabase? = null
        fun getInstance(): KupacDatabase {
            if(INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    MyApplication.ApplicationContext,
                        KupacDatabase::class.java,
                    NAME
                )
                    .allowMainThreadQueries()
                    .build()
            }
            return INSTANCE as KupacDatabase
        }
    }
}