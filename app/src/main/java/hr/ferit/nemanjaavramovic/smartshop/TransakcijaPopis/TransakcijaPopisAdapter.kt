package hr.ferit.nemanjaavramovic.smartshop.TransakcijaPopis

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton
import com.squareup.picasso.Picasso
import hr.ferit.nemanjaavramovic.smartshop.Proizvod.ProizvodKosarica
import hr.ferit.nemanjaavramovic.smartshop.Proizvod.ProizvodKosaricaInteractionListener
import hr.ferit.nemanjaavramovic.smartshop.R
import kotlinx.android.synthetic.main.proizvod_kosarica_layout.view.*
import kotlinx.android.synthetic.main.transakcija_layout.view.*


class TransakcijaPopisAdapter(transakcijaPopis: MutableList<TransakcijaPopis>,
                              transakcijaPopisListener: TransakcijaPopisInteractionListener
) : RecyclerView.Adapter<TransakcijaPopisAdapter.TransakcijaPopisHolder>(){


    private val transakcijaPopis: MutableList<TransakcijaPopis>
    private val transakcijaPopisListener: TransakcijaPopisInteractionListener

    init {
        this.transakcijaPopis = mutableListOf()
        this.transakcijaPopis.addAll(transakcijaPopis)
        this.transakcijaPopisListener = transakcijaPopisListener
    }


    fun refreshData(transakcijaPopis: MutableList<TransakcijaPopis>) {
        this.transakcijaPopis.clear()
        this.transakcijaPopis.addAll(transakcijaPopis)
        this.notifyDataSetChanged()
    }

    override fun getItemCount(): Int = transakcijaPopis.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransakcijaPopisHolder {
        val transakcijeView = LayoutInflater.from(parent.context).inflate(R.layout.transakcija_layout, parent, false)
        return TransakcijaPopisHolder(
            transakcijeView
        )
    }


    override fun onBindViewHolder(holder: TransakcijaPopisHolder, position: Int) {
        val transakcija = transakcijaPopis[position]
        holder.bind(transakcija, transakcijaPopisListener)
    }


    class TransakcijaPopisHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(transakcija: TransakcijaPopis, transakcijaPopisListener: TransakcijaPopisInteractionListener) {
            itemView.imeKupca.text = transakcija.imeKupca
            itemView.prezimeKupca.text = transakcija.prezimeKupca
            itemView.ukupnaCijenatxt.text = "Ukupna cijena: " + transakcija.ukupnaCijena +"kn"
            itemView.datumTxt.text = "Datum: " + transakcija.datum + ","
            itemView.vrijemeTxt.text = "Vrijeme: " + transakcija.vrijeme
        }
    }
}