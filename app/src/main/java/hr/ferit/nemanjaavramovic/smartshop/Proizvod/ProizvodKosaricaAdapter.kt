package hr.ferit.nemanjaavramovic.smartshop.Proizvod

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton
import com.squareup.picasso.Picasso
import hr.ferit.nemanjaavramovic.smartshop.R
import kotlinx.android.synthetic.main.proizvod_kosarica_layout.view.*

class ProizvodKosaricaAdapter(proizvodiKosarica: MutableList<ProizvodKosarica>,
                              proizvodiKosaricaListener: ProizvodKosaricaInteractionListener
) : RecyclerView.Adapter<ProizvodKosaricaAdapter.ProizvodiKosaricaHolder>(){


    private val proizvodiKosarica: MutableList<ProizvodKosarica>
    private val proizvodiKosaricaListener: ProizvodKosaricaInteractionListener
    private var totalPrice : Double = 0.0

    init {
        this.proizvodiKosarica = mutableListOf()
        this.proizvodiKosarica.addAll(proizvodiKosarica)
        this.proizvodiKosaricaListener = proizvodiKosaricaListener
    }


    fun refreshData(proizvodiKosarica: MutableList<ProizvodKosarica>) {
        this.proizvodiKosarica.clear()
        this.proizvodiKosarica.addAll(proizvodiKosarica)
        this.notifyDataSetChanged()
    }

    override fun getItemCount(): Int = proizvodiKosarica.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProizvodiKosaricaHolder {
        val proizvodiView = LayoutInflater.from(parent.context).inflate(R.layout.proizvod_kosarica_layout, parent, false)
        return ProizvodiKosaricaHolder(
            proizvodiView
        )
    }


    override fun onBindViewHolder(holder: ProizvodiKosaricaHolder, position: Int) {
        val proizvod = proizvodiKosarica[position]
        /*val onePrice : Double = proizvod.proizvodCijena * proizvod.proizvodKolicina
        totalPrice = totalPrice + onePrice*/
        holder.bind(proizvod, proizvodiKosaricaListener)
    }


    class ProizvodiKosaricaHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(proizvod: ProizvodKosarica, proizvodiKosaricaListener: ProizvodKosaricaInteractionListener) {
            itemView.proizvodKosaricaIme.text = proizvod.proizvodIme
            Picasso.get().load(proizvod.proizvodSlika).into(itemView.proizvodKosaricaSlika)
            //itemView.proizvodKosaricaSlika.text=proizvod.proizvodSlika
            itemView.proizvodKosaricaCijena.text=proizvod.proizvodCijena.toString()
            itemView.proizvodKosaricaKolicina.number = proizvod.proizvodKolicina.toString()
            itemView.proizvodKosaricaKolicina.setOnClickListener (ElegantNumberButton.OnClickListener{proizvodiKosaricaListener.onUpdate(proizvod.proizvodIme,itemView.proizvodKosaricaKolicina.number)})
            //itemView.image.setBackgroundColor(task.taskPriority)
            //itemView.proizvod_kosarica_layout.setOnClickListener{proizvodiKosaricaListener.onUpdate(proizvod.proizvodIme)}
            itemView.proizvod_kosarica_layout.setOnLongClickListener{proizvodiKosaricaListener.onDelete(proizvod.proizvodIme)
                true}
        }
    }
}