package hr.ferit.nemanjaavramovic.smartshop.UserActivities

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import hr.ferit.nemanjaavramovic.smartshop.Proizvod.ProizvodKosaricaAdapter
import hr.ferit.nemanjaavramovic.smartshop.Proizvod.ProizvodKosaricaDao
import hr.ferit.nemanjaavramovic.smartshop.Proizvod.ProizvodKosaricaDatabase
import hr.ferit.nemanjaavramovic.smartshop.Proizvod.ProizvodKosaricaInteractionListener
import hr.ferit.nemanjaavramovic.smartshop.R
import kotlinx.android.synthetic.main.activity_kosarica.*
import java.math.RoundingMode

class KosaricaActivity : AppCompatActivity() {

    companion object {
        lateinit var proizvodKosaricaDatabase: ProizvodKosaricaDao
    }

    private var cijena: TextView? = null

    var ukupnaCijena : Double= 0.00

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kosarica)
        setUpUi()
        prikaziProizvode()
    }



    @SuppressLint("WrongConstant")
    private fun setUpUi() {
        proizvodKosaricaDatabase = ProizvodKosaricaDatabase.getInstance().proizvodKosaricaDao()

        recycler_view.layoutManager= LinearLayoutManager(this, LinearLayout.VERTICAL,false)

        platiBtn.setOnClickListener{otvoriPlacanje()}
    }

    private fun otvoriPlacanje() {
        val num = proizvodKosaricaDatabase.getAll().size

        if (num > 0){
            val displayIntent = Intent(this, PlacanjeActivity::class.java)
            displayIntent.putExtra(PlacanjeActivity.UKUPNA_CIJENA, ukupnaCijena)
            startActivity(displayIntent)
        }
        else{
            Toast.makeText(this@KosaricaActivity, "Košarica je prazna",
                Toast.LENGTH_LONG).show()
        }

    }

    @SuppressLint("SetTextI18n")
    private fun prikaziProizvode() {
        val proizvodiKosaricaListener = object:
            ProizvodKosaricaInteractionListener {
            override fun onUpdate(proizvodIme: String, number: String) {
                funUpdate(proizvodIme, number)
            }
            override fun onDelete(proizvodIme: String) {
                funDelete(proizvodIme)
            }
        }
        cijena = findViewById<View>(R.id.cijena) as TextView
        val num = proizvodKosaricaDatabase.getAll().size
        ukupnaCijena = 0.0
        var i = 0
        while(i < num){
            ukupnaCijena = ukupnaCijena + (proizvodKosaricaDatabase.getAll()[i].proizvodCijena * proizvodKosaricaDatabase.getAll()[i].proizvodKolicina)
            i++
        }
        ukupnaCijena = ukupnaCijena.toBigDecimal().setScale(2, RoundingMode.UP).toDouble()
        cijena!!.text = ukupnaCijena.toString() + "kn"
        recycler_view.adapter=
            ProizvodKosaricaAdapter(
                proizvodKosaricaDatabase.getAll(),
                proizvodiKosaricaListener
            )
    }

    private fun funUpdate(proizvodIme: String, number: String) {
        var proizvod = proizvodKosaricaDatabase.getByName(proizvodIme)
        proizvod.proizvodKolicina = number.toInt()
        proizvodKosaricaDatabase.update(proizvod)
        prikaziProizvode()
    }



    private fun funDelete(proizvodIme: String) {
        var proizvod = proizvodKosaricaDatabase.getByName(proizvodIme)
        val proizvodIme = proizvod.proizvodIme
        val alertDialog = AlertDialog.Builder(this@KosaricaActivity)
            .setTitle("Upozorenje")
            .setMessage("Da li ste sigurni da zelite izbrisati proizvod '$proizvodIme' iz kosarice?")
            .setPositiveButton("Izbrisi", { dialog, which ->
                proizvodKosaricaDatabase.delete(proizvod)
                prikaziProizvode()})
            .setNegativeButton("Odustani", {dialog, which ->
                prikaziProizvode()
            })
        alertDialog.show()
    }


    @SuppressLint("MissingSuperCall")
    override fun onResume(){
        prikaziProizvode()
        super.onResume()
    }
}
