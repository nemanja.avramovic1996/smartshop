package hr.ferit.nemanjaavramovic.smartshop


import androidx.room.*

@Dao
interface ProizvodKosaricaDao{
    @Insert
    fun insert(proizvodKosarica: ProizvodKosarica);
    @Delete
    fun delete(proizvodKosarica: ProizvodKosarica);
    @Update
    fun update(proizvodKosarica: ProizvodKosarica);
    @Query("SELECT * FROM proizvodi")
    fun getAll(): MutableList<ProizvodKosarica>;
    @Query("SELECT * FROM proizvodi WHERE proizvodIme = :proizvodIme")
    fun getByName(proizvodIme: String): ProizvodKosarica;
}