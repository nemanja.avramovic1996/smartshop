package hr.ferit.nemanjaavramovic.smartshop



import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(version = 4, entities = arrayOf(Kupac::class))
abstract class KupacDatabase : RoomDatabase() {
    abstract fun kupacDao(): KupacDao
    companion object {
        private const val NAME = "kupac_database"
        private var INSTANCE: KupacDatabase? = null
        fun getInstance(): KupacDatabase {
            if(INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                        ProizvodKosaricaApplication.ApplicationContext,
                        KupacDatabase::class.java,
                        NAME)
                    .allowMainThreadQueries()
                    .build()
            }
            return INSTANCE as KupacDatabase
        }
    }
}