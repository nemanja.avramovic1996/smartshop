package hr.ferit.nemanjaavramovic.smartshop

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_admin.*

class AdminActivity : AppCompatActivity() {
    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null
    private var mAuth: FirebaseAuth? = null
    private var btnOdjava: Button? = null
    private var btnTrgovina: Button? = null

    var kupacDatabase = KupacDatabase.getInstance().kupacDao()
    //val kupacDatabase = KupacDatabase.getInstance().kupacDao()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)
        inicijalizacija()
        //prikaziKupce()

    }



    @SuppressLint("WrongConstant")
    private fun inicijalizacija() {
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("Korisnici")
        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance()

        btnOdjava = findViewById<View>(R.id.adminOdjavaBtn) as Button
        btnTrgovina = findViewById<View>(R.id.trenutnoUTrgoviniBtn) as Button
        btnOdjava?.setOnClickListener { odjaviteSe() }
        btnTrgovina?.setOnClickListener { pogledajTkoJeUTrgovini() }

        dodajProizvodBtn.setOnClickListener { dodajNoviProizvod() }






        //recycler_view_kupci.layoutManager= LinearLayoutManager(this, LinearLayout.VERTICAL,false)
    }

    private fun dodajNoviProizvod() {
        startActivity(Intent(this@AdminActivity,
            DodajNoviProizvodActivity::class.java))
    }

    /*private fun popuniTablicu() {

    }*/

    private fun pogledajTkoJeUTrgovini() {
        startActivity(Intent(this@AdminActivity,
            TrenutnoUTrgoviniActivity::class.java))
    }


    /*private fun prikaziKupce() {
        val kupacListener = object: KupacInteractionListener{
        }

        /*val kupac = Kupac(0,"Nemanja","Avramovic")
        kupacDatabase.insert(kupac)*/

        popuniTablicu()
        recycler_view_kupci.adapter= KupacAdapter(kupacDatabase.getAll(),kupacListener)
    }*/


    private fun odjaviteSe(){

        var sviKupci = kupacDatabase.getAll()
        for (k in sviKupci){
            kupacDatabase.delete(k)
        }
        mAuth?.signOut()
        startActivity(
            Intent(this,
                LoginActivity::class.java)
        )
    }

    override fun onStart() {
        super.onStart()
        val mUser = mAuth!!.currentUser
        val mUserReference = mDatabaseReference!!.child(mUser!!.uid)
        //tvEmail!!.text = mUser.email
        mUserReference.addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {

            }
            override fun onCancelled(databaseError: DatabaseError) {}
        })


        mDatabaseReference?.addValueEventListener(object : ValueEventListener {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(snapshot: DataSnapshot) {
                for(k in snapshot.children){
                    if(k.child("trenutno_u_trgovini").value =="+" ){
                        val kupacIme = k.child("ime").value as String
                        val kupacPrezime = k.child("prezime").value as String
                        val kupac = Kupac(0,kupacIme,kupacPrezime)
                        kupacDatabase.insert(kupac)
                    }

                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    override fun onBackPressed() {
        Toast.makeText(this, "Ne mozete ici unazad, prvo se morate odjaviti",
            Toast.LENGTH_LONG).show()
    }



}
