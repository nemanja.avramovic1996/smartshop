package hr.ferit.nemanjaavramovic.smartshop.TransakcijaPopis

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "transakcije")
data class TransakcijaPopis(
    @PrimaryKey(autoGenerate = true) var id: Int,
    @ColumnInfo(name = "imeKupca") var imeKupca: String,
    @ColumnInfo(name = "prezimeKupca") var prezimeKupca: String,
    @ColumnInfo(name = "ukupnaCijena") var ukupnaCijena: String,
    @ColumnInfo(name = "datum") var datum: String,
    @ColumnInfo(name = "vrijeme") var vrijeme: String
    )