package hr.ferit.nemanjaavramovic.smartshop.SviProizvodi

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import hr.ferit.nemanjaavramovic.smartshop.MyApplication


@Database(version = 4, entities = arrayOf(SviProizvodi::class))
abstract class SviProizvodiDatabase : RoomDatabase() {
    abstract fun sviProizvodiDao(): SviProizvodiDao
    companion object {
        private const val NAME = "svi_proizvodi__database"
        private var INSTANCE: SviProizvodiDatabase? = null
        fun getInstance(): SviProizvodiDatabase {
            if(INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    MyApplication.ApplicationContext,
                    SviProizvodiDatabase::class.java,
                    NAME
                )
                    .allowMainThreadQueries()
                    .build()
            }
            return INSTANCE as SviProizvodiDatabase
        }
    }
}